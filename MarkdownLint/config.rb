require 'mixlib/config'

module MarkdownLint
  module Config
    extend Mixlib::Config
rules "MD001",          "MD003", "MD004",             "MD006", "MD007", "MD008", "MD009", 
"MF010", "MD011", "MD012", "MD013", "MD014", "MD015", "MD016", "MD017", "MD018", "MD019", 
"MD020", "MD021", "MD022", "MD023",          "MD025", "MD026", "MD027", "MD028", "MD029", 
"MD030", "MD031", "MD032",          "MD034", "MD035", "MD036", "MD037", "MD038", "MD039", 
"MD040", "MD041", "MD042", "MD043", "MD044", "MD045"
    default :style, "default"


  end
end
