# Author: Hanley Lee
# Website: https://www.hanleylee.com
# GitHub: https://github.com/HanleyLee

# VCS
YS_VCS_PROMPT_PREFIX1=" %{$fg[white]%}on%{$reset_color%} "
YS_VCS_PROMPT_PREFIX2=":%{$fg[cyan]%}"
YS_VCS_PROMPT_SUFFIX="%{$reset_color%}"
YS_VCS_PROMPT_DIRTY=" %{$fg[red]%}x"
YS_VCS_PROMPT_CLEAN=" %{$fg[green]%}o"

# Git info
local git_info='$(git_prompt_info)'
ZSH_THEME_GIT_PROMPT_PREFIX="${YS_VCS_PROMPT_PREFIX1}git${YS_VCS_PROMPT_PREFIX2}"
ZSH_THEME_GIT_PROMPT_SUFFIX="$YS_VCS_PROMPT_SUFFIX"
ZSH_THEME_GIT_PROMPT_DIRTY="$YS_VCS_PROMPT_DIRTY"
ZSH_THEME_GIT_PROMPT_CLEAN="$YS_VCS_PROMPT_CLEAN"

# HG info
local hg_info='$(ys_hg_prompt_info)'
ys_hg_prompt_info() {
	# make sure this is a hg dir
	if [ -d '.hg' ]; then
		echo -n "${YS_VCS_PROMPT_PREFIX1}hg${YS_VCS_PROMPT_PREFIX2}"
		echo -n $(hg branch 2>/dev/null)
		if [ -n "$(hg status 2>/dev/null)" ]; then
			echo -n "$YS_VCS_PROMPT_DIRTY"
		else
			echo -n "$YS_VCS_PROMPT_CLEAN"
		fi
		echo -n "$YS_VCS_PROMPT_SUFFIX"
	fi
}

local exit_code="%(?,,C:%{$fg[red]%}%?%{$reset_color%})"

# 在屏幕右侧显示命令时间
local hl_time_on_right='$(align_right " `get_date_time`")'
align_right() {
    local str="$1"
    local align_site=`get_space_size "$str"`
    local cursor_back="\033[${align_site}G"
    local cursor_begin="\033[1G"
    echo "${cursor_back}${str}${cursor_begin}"
}
get_date_time() {
    # echo "%{$reset_color%}%D %*"
    command date "+%H:%M:%S"
}
get_space_size() {
    # ref: http://zsh.sourceforge.net/Doc/Release/Expansion.html#Parameter-Expansion-Flags
    local str="$1"
    local zero_pattern='%([BSUbfksu]|([FB]|){*})'
    local len=${#${(S%%)str//$~zero_pattern/}}
    local size=$(( $COLUMNS - $len ))
    echo "$size"
}

# Prompt format:
# PRIVILEGES USER @ MACHINE in DIRECTORY on git:BRANCH STATE [TIME] C:LAST_EXIT_CODE
# $ COMMAND

# For example:
# % ys in ~/.oh-my-zsh on git:master x [21:47:42] C:0
# $
PROMPT="
%(#,$fg[yellow]%}%n%{$reset_color%},%{$fg[yellow]%}%n) \
%{$terminfo[bold]$fg[cyan]%}%~%{$reset_color%}\
${hg_info}\
$git_info  $exit_code  %{$terminfo[regular]$fg[white]%}$hl_time_on_right
%{$terminfo[bold]$fg[green]%}$ %{$reset_color%}"

