#██████████████████████   Function   ██████████████████████████

# 选择 zsh 位置为 on-my-sh
export ZSH="/Users/hanley/.oh-my-zsh"

# 向系统环境路径中添加自定义路径
export PATH="/usr/local/sbin:$PATH"

# 禁用 Homebrew 每次安装软件时的更新
export HOMEBREW_NO_AUTO_UPDATE=true

EDITOR=vim
export EDITOR
# 设置所有字符编码格式为 UTF-8
export LC_ALL=en_US.UTF-8

#██████████████████████   Appearance   ██████████████████████████

# 使用自定义主题
ZSH_THEME="hl"

# history时间格式更改
HIST_STAMPS="yyyy-mm-dd"


#██████████████████████   Plugin   ██████████████████████████
# 配置 plugins
plugins=(
    zsh-autosuggestions
    last-working-dir
    z
    osx
    command-not-found
)

#████████████████████████████████████████████████████████████
# 调用 zsh, 必须放在插件配置之后, 启用效果之前, 否则插件能用, 效果不会显示
source $ZSH/oh-my-zsh.sh


#██████████████████████   alia    ███████████████████████████
# 设置自定义 alias, 必须放在 source zsh 之后
# 将 Homebrew 安装的 python3 的别名设置为 python
# alias python="/usr/local/bin/python3"
alias vim='mvim -v'
alias rm='trash'
alias l='lsd -l'

# 启用 zsh 自带高亮效果
source /Users/hanley/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

# 绑定autosuggestions补全快捷键为逗号 ,
bindkey ',' autosuggest-accept
ZSH_AUTOSUGGEST_HIGHLIGHT_STYLE="fg=#8c8c8c,underline" # 提示样式, 可设置前景, 背景, 加粗, 下划线

# 设定 nvm, 如果不设置的话不能使用 nvm 及其命令
source $(brew --prefix nvm)/nvm.sh

test -e "${HOME}/.iterm2_shell_integration.zsh" && source "${HOME}/.iterm2_shell_integration.zsh"

