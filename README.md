# HLConfigs

![GitHub last commit](https://img.shields.io/github/last-commit/hanleylee/HLConfigs)
![GitHub](https://img.shields.io/github/license/hanleylee/HLConfigs)

本仓库用于分享开发者 [Hanley](https://www.hanleylee.com) 在开发过程中工具软件的配置. 每个人都有自己的配置, 但是有相互借鉴才会有进步, 在此列出本人的配置, 用于大家参考. 目前涉及软件有:

1. `vim`
2. `Karabiner-Elements`
3. `zsh`
4. `Markdown Lint Tool`
5. `Tmux`
6. `XVim`

## Vim

正如《程序员修炼之道》一书中所说:

> 最好是精通一种编辑器，并将其用于所有编辑任务。如果不坚持使用一种编辑器，可能会面临现代的巴别特大混乱。

作为一个有远大志向的程序员, 我选择有着悠久历史且长盛不衰的编辑器 `vim`, `vim` 的每个快捷键设计都很有深度, 在熟练掌握大部分常用快捷键后能让我们在编程时更加专注不被打断思路, 能有效提高编程效率.

### 已配置插件

| 插件名                                                         | 作用                                                                                                |
|:---------------------------------------------------------------|:----------------------------------------------------------------------------------------------------|
| scrooloose/nerdtree                                            | 目录树显示                                                                                          |
| Xuyuanp/nerdtree-git-plugin                                    | 目录树 git 状态显示                                                                                 |
| lyokha/vim-xkbswitch', {'as': 'xkbswitch'}                     | 返回到 normal 模式时快速切换为英文输入法                                                            |
| godlygeek/tabular                                              | 文本对齐(Markdown 语法插件的基础), 使用:Tabularize /=可以等号对齐多行. 必须安装在 Markdown 插件前方 |
| iamcco/markdown-preview.nvim', {'do': 'cd app & yarn install'} | Markdown 实时预览                                                                                   |
| Yggdroot/LeaderF                                               | 快速搜索文件                                                                                        |
| powerline/powerline                                            | 底部状态栏显示                                                                                      |
| scrooloose/syntastic                                           | 语法错误检查                                                                                        |
| nvie/vim-flake8                                                | 对 python 进行拼写检查, 与 syntastic 一起工作                                                       |
| vim-scripts/indentpython.vim                                   | 自动缩进 python 代码                                                                                |
| ycm-core/YouCompleteMe                                         | 补全插件                                                                                            |
| jszakmeister/markdown2ctags                                    | markdown 层级显示                                                                                   |
| majutsushi/tagbar                                              | 显示文档的层级                                                                                      |
| junegunn/seoul256.vim                                          | Vim 配色(韩国人审美)                                                                                |
| dhruvasagar/vim-table-mode                                     | Markdown 自动表格, 使用`\tm`就进入了表格模式, 会进行自动对齐                                        |
| mhinz/vim-startify                                             | 首页显示                                                                                            |
| qpkorr/vim-renamer                                             | 批量修改文件的神器, 使用 :Ren 进行编辑与保存, 完成后退出即可                                        |
| sheerun/vim-polyglot                                           | 高亮语法显示插件, 显示各种语言高亮                                                                  |
| blockloop/vim-swigjs                                           | 显示 swig 语法高亮                                                                                  |
| jiangmiao/auto-pairs                                           | 匹配括号                                                                                            |
| nathanaelkane/vim-indent-guides                                | 缩进线显示                                                                                          |

### 自定义设置

- 将`vim` 普通模式下 `d` `x` `c` `s` 的剪贴性质改为删除性质, 剪贴操作全部放在`Visual`模式下进行
- 将`vim`的默认存储器由寄存器改为系统剪贴板, 方便复制粘贴
- 设置了插入模式下的方向键映射
- 设置了`Python` `C` `C++` `Java` `Go` `HTML`语言的快速执行命令
- ...

## Karabiner-Elements

`vim` 的绝大部分快捷键只在 `vim` 环境下可以使用, 如果偶尔需要在其他环境下进行轻量文本编辑工作的话快捷键的切换会让人很痛苦. `Karabiner` 是 `Mac` 操作系统下一个很好的开源软件, 可以让我们将在 `vim` 中已经熟悉的大部分文本操作命令映射到别的文本编辑环境中.

[Karabiner 官网](https://pqrs.org/osx/karabiner/)

Karabiner-Element 配置文件路径: `/Users/hanley/.config/karabiner/assets/complex_modifications`

### 映射键位

| 按键    | 作用                       |
|:--------|:---------------------------|
| `⌃ j`   | 光标向下 ↓ 移动一个字符    |
| `⌃ k`   | 光标向上 ↑ 移动一个字符    |
| `⌃ l`   | 光标向右 → 移动一个字符    |
| `⌃ h`   | 光标向左 ← 移动一个字符    |
| `⌃ o`   | 在当前行下方插入一行       |
| `⌃ ⇧ o` | 在当前行上方插入一行       |
| `⌃ d`   | 向下翻一页                 |
| `⌃ u`   | 向上翻一页                 |

### 说明

- 此配置完全是希望能够模拟 vim 使用习惯, 对于不使用 vim 的人来说没有参考意义
- 此配置全部基于⌃(Control) 按键, 有以下原因
  1. Mac 系统及系统中软件使用 ⌃(Control) 键作为修饰的命令不多, 可避免冲突
  2. 键位统一, 便于记忆
  3. 本人所使用键盘是 HHKB, ⌃(Control) 键比较容易触及
- 此配置的产生完全基于作者个人习惯, 构思这些键位也花费了作者大量精力, 但是可能仍然不能符合你的使用习惯, 但是我相信此配置仍然可以作为你自定义你自己配置的参考.
- 此配置排除了`Terminal`, `iTerm`, `Xcode` 的使用环境, 因为再这些环境下的编辑已经可以使用 vim, 不需要进行再度模拟.

ps: 刚开始觉得按键映射的越多越好, 随着使用时间越来越长发现会和很多软件的快捷键冲突. 仔细想了一下, 也就只有方向键和翻页键用的最为频繁, 所以只留下这几个就可以了.

## Zsh(Z shell)

一款 shell, Mac 内置, 与Mac 默认使用的 bash 相比具有更强大的自动判断和识别功能, 同时具有较多插件可以选择. 完全兼容 bash 的所有特性和操作

使用了 oh-my-zsh 配置集, 基于`ys`主题修改了自定义主题`hl.zsh_theme`, 效果如下:

<p align="center">
<a href="https://www.hanleylee.com">
<img src="https://a.hanleylee.com/HKMS/2020-02-12-134208.png?x-oss-process=style/WaMa" 
style="width: 888px;" 
alt="Hanley Lee"
title="Hanley"></a>
<br/>
自定义 zsh 主题 hl, 简洁
<br/>
<p>

### 已配置 Zsh 插件

| 插件名                  | 作用                                     |                             官网                             |
|:------------------------|:-----------------------------------------|:------------------------------------------------------------:|
| zsh-autosuggestions     | 自动提示历史命令                         |   [官网](https://github.com/zsh-users/zsh-autosuggestions)   |
| zsh-syntax-highlighting | 自动高亮语法                             | [官网](https://github.com/zsh-users/zsh-syntax-highlighting) |
| last-working-dir        | 进入 zsh 时自动进入上次离开 zsh 时的目录 |                               -                              |

## Markdown Lint Tool(语法检查)

根据已设定的规则检查 Markdown 语法错误, 可以根据需要自定义规则

如果配合 synatics 插件使用的话可以在 vim 中看到实时的错误情况, 而不是只能在终端中输出错误

### 安装

根据 [官网](https://github.com/markdownlint/markdownlint) 流程, 直接使用 Gem 进行安装

```bash
gem install mdl
```

### 使用

```vim
mdl <filename>
```

### 配置规则

默认的规则可能不适合中国人写作, 比如单段超过 80 是字符就会报错显然是不合理的, 按照官网指导可以在直接修改 gem 文件夹下的`rules.rb`文件. 其地址位于`/Library/Ruby/Gems/2.6.0/gems/mdl-0.8.0/lib/mdl/rules.rb`

除了自定义`rules.rb`, 还可以在同路径下的`config.rb`中设置需要的风格, rule, tag, 警告等, 具体设置参考[官方指导](https://github.com/markdownlint/markdownlint/blob/master/docs/configuration.md)

## Tmux

Tmux 是一款终端复用工具, 相比起 Terminal 或 iTerm, Tmux 可以使用快捷键方便的切换会话窗口, 保存恢复会话, 以及方便地进行远程会话. 具体效果如下:

![himg](https://a.hanleylee.com/HKMS/2020-01-19-060632.png?x-oss-process=style/WaMa)

### 安装

```bash
brew install tmux
```

### 已配置插件

| 插件名              | 作用                |                          官网                          |
|:--------------------|:--------------------|:------------------------------------------------------:|
| Tmux Plugin Manager | Tmux 插件安装, 管理 |       [官网](https://github.com/tmux-plugins/tpm)      |
| Tmux Resurrect      | Tmux 会话保存恢复   | [官网](https://github.com/tmux-plugins/tmux-resurrect) |

## XVim

XVim 是 vim 的 Xcode 插件，可以让 Xcode 向 vim 一样编辑。

[官网](https://github.com/XVimProject/XVim2)

## 开源许可

本仓库的所有代码基于 [Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0) 进行分发与使用. 协议全文见 [LICENSE](https://github.com/HanleyLee/HLConfigs/blob/master/LICENSE) 文件.

Copyright 2019-2020 HanleyLee

---

欢迎使用, 有任何 bug, 希望给我提 issues. 如果对你有用的话请标记一颗星星 ⭐️
