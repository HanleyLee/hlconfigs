"Author: Hanley Lee
"Website: https://www.hanleylee.com
"GitHub: https://github.com/HanleyLee

"███████████████████████   Vim 系统特性   ██████████████████████████

"=======================   Main   ==================================
set nocompatible "关闭 vi 兼容模式, 必选
filetype off "required
filetype plugin indent on "开启插件功能,必选
set shell=/bin/bash "不要设置为 zsh, 否则在 GUI 下进入 shell 时会乱码
set modifiable "设置文件可被修改
set history=1000 "历史命令最大记录数

"======================   Copy & Paste   ===========================
set mouse+=a "保证行号不被复制
set clipboard=unnamed "保证只按下 y 即可复制到系统剪贴板, 否则需要 "+y 三个按键

"=======================   Appearance   ============================
syntax on "设置语法高亮
set number "显示行号
set guicursor+=a:blinkon0 " 设置光标不闪动
set cursorline "突出光标所在行
set cursorcolumn "突出光标所在列
set wildmenu "输入部分命令按下 tab 键可以弹出符合的命令列表
set scrolloff=3 "设置光标移动到顶部的距离保持三行
set showcmd "右下角显示正在操作的命令
set list "设置显示行尾, 换行, 制表符等隐藏字符
set listchars=tab:▸-,eol:↩︎,trail:-,space:⋅ "自定义换行, 制表符等显示格式
set laststatus=2 "必须设置, 否则 powerline 不能正确显示
set rtp+=~/.vim/plugged/powerline/powerline/bindings/vim "设置 powerline 路径
set showtabline=2 "始终显示窗口上头的tabline
set noshowmode "隐藏vim 的默认提示当前状态信息, eg. 在状态栏下显示'insert', 'replace'等信息
set t_Co=256 "Vim 能显示的颜色数
set guifont=YaHei\ Consolas\ Icon\ Hybrid:h16 "设置 GUI 下字体及大小, 针对 MacVim 进行设置
set guifontwide=YaHei\ Consolas\ Icon\ Hybrid:h14 "设置 GUI 下中文字体及大小, 针对 MacVim 进行设置


"=======================   Search   ================================
set hlsearch "搜索结果高亮
set incsearch "搜索时输入内容时立刻进行搜索
set ignorecase "查找时忽略大小写
set smartcase "搜索时只有输入大写字母时才会强制符合大小写, 需要与 ignorecase 一同设置才能正常工作
set showmatch "匹配括号

"=======================   Indent   ================================
set backspace=2 "Backspace 键是否可以删除字符
set autoindent "自动缩进, 需要与 smartindent 配合使用才有效果
set smartindent "智能缩进, 在进行代码编写的时候会智能判断缩进距离, 与 autoindent 配合使用
set smarttab
set shiftwidth=4 "设置缩进的字符数, 与 tabstop 不同, 这个是系统进行自动缩进时会使用的值
set tabstop=4 "按下一次 tab 键缩进的距离, 不代表空格或制表符
set expandtab "缩进使用空格代表, 如果要使用制表符可以改为 noexpandtab, 设置后旧文件仍然采用之前的 tab 格式, 可以使用`%retab!`强制转换为新tab 格式

"=======================   File   =======================
set autochdir "自动将当前编辑文件的路径变为工作目录(比如用于 args 批量操作)
set fileformat=unix "保存文件的格式为 unix
set fileencodings=utf-8,ucs-bom,gb18030,gbk,gb2312,cp936
set encoding=utf-8 "UTF-8 支持
set termencoding=utf-8
set fenc=utf-8 "编码方式为 utf-8
"set autowriteall "切换文件时自动保存

"███████████████████████   KeyMapping   ██████████████████████████

" 将默认普通模式下的的 d x s c 剪切命令全部改为删除,如果需要剪切进入可视 式操作
vnoremap d "_d
nnoremap d "_d
nnoremap dd "_dd
nnoremap D "_D


nnoremap x "_x
nnoremap X "_X

nnoremap c "_c
nnoremap C "_C
nnoremap cc "_cc

"nnoremap s "_s
"nnoremap S "_S

" 使j和k命令自动判断是在折行内或是在折行间进行移动
"noremap j v:count == 0 ? 'gj' : 'j'
"noremap k v:count == 0 ? 'gk' : 'k'

" 映射插入模式下的方向键
"inoremap <C-j> <Down>
"inoremap <C-k> <Up>
"inoremap <C-h> <Left>
"inoremap <C-l> <Right>

" 使用⌃ G 快速查看当前文件路径
map <C-g> :echo expand('%:p')<CR>

" 使用 F5 快速执行代码      
map <F5> :call Run()<CR>
func! Run()
    exec "w"
    if &filetype == 'c'
                exec "!g++ % -o %<"
                exec "!time ./%<"
    elseif &filetype == 'cpp'
                exec "!g++ % -o %<"
                exec "!time ./%<"
    elseif &filetype == 'java'
                exec "!javac %"
                exec "!time java %<"
    elseif &filetype == 'sh'
                :!time bash %
    elseif &filetype == 'python'
            exec "!time python3 %"
    elseif &filetype == 'html'
                exec "!firefox % &"
    elseif &filetype == 'go'
        "        exec "!go build %<"
                exec "!time go run %"
    elseif &filetype == 'mkd'
                exec "!~/.vim/markdown.pl % > %.html &"
                exec "!firefox %.html &"
    endif
endfunc

" 格式化 JSON 命令, 可适配中文
command! Jsonf :execute '%!python -m json.tool'
  \ | :execute '%!python -c "import re,sys;sys.stdout.write(re.sub(r\"\\\u[0-9a-f]{4}\", lambda m:m.group().decode(\"unicode_escape\").encode(\"utf-8\"), sys.stdin.read()))"'

"设置PEP8风格的 python 及 js 代码
au BufNewFile,BufRead *.py set tabstop=4 |set softtabstop=4|set shiftwidth=4|set textwidth=79|set expandtab|set autoindent|set fileformat=unix
au BufNewFile,BufRead *.js,*.html,*.css set tabstop=2|set softtabstop=2|set shiftwidth=2

"自动跳转到上次退出的位置
if has("autocmd")
  au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif
endif

"███████████████████████   插件   ██████████████████████████

"vim-plug 插件管理工具使用命令
" :PlugInstall    -  安装插件
" :PlugUpdate     -  更新插件
" :PlugUpgrade    -  更新 vim-plug 本身
" :PlugClean      -  清理不使用的插件

call plug#begin('~/.vim/plugged')

Plug 'scrooloose/nerdtree' "目录树显示
Plug 'Xuyuanp/nerdtree-git-plugin' "目录树 git 状态显示
Plug 'lyokha/vim-xkbswitch', {'as': 'xkbswitch'} "返回到 normal 模式时快速切换为英文输入法
Plug 'godlygeek/tabular' "文本对齐(Markdown 语法插件的基础), 使用:Tabularize /=可以等号对齐多行. 必须安装在 Markdown 插件前方
Plug 'iamcco/markdown-preview.nvim', {'do': 'cd app & yarn install'} "Markdown 实时预览
Plug 'Yggdroot/LeaderF' "快速搜索文件
Plug 'powerline/powerline' "底部状态栏显示
Plug 'scrooloose/syntastic' "语法错误检查
Plug 'nvie/vim-flake8' "对 python 进行拼写检查, 与 syntastic 一起工作
Plug 'vim-scripts/indentpython.vim' "自动缩进 python 代码
Plug 'ycm-core/YouCompleteMe' "补全插件
Plug 'jszakmeister/markdown2ctags' "markdown 层级显示
Plug 'majutsushi/tagbar' "显示文档的层级
Plug 'junegunn/seoul256.vim' "Vim 配色(韩国人审美)
Plug 'dhruvasagar/vim-table-mode' "Markdown 自动表格, 使用`\tm`就进入了表格模式, 会进行自动对齐
Plug 'mhinz/vim-startify' "首页显示
Plug 'qpkorr/vim-renamer' "批量修改文件的神器, 使用 :Ren 进行编辑与保存, 完成后退出即可
Plug 'sheerun/vim-polyglot' "高亮语法显示插件, 显示各种语言高亮
Plug 'blockloop/vim-swigjs' "显示 swig 语法高亮
Plug 'nathanaelkane/vim-indent-guides' "缩进线显示
Plug 'tpope/vim-surround' "包围符号
Plug 'jiangmiao/auto-pairs' "匹配括号
"Plug 'junegunn/goyo.vim' "专注于写作的插件, 不适用写代码!
"Plug 'junegunn/limelight.vim' "暗光效果插件, 与 Goyo 联动配合使用效果最佳
"Plug 'hotoo/pangu.vim' "中文自动排版
"Plug 'elzr/vim-json'
"Plug 'keith/swift.vim' "swift 语法显示
"Plug 'xolox/vim-misc'
"
call plug#end()

"*****************   NerdTree 目录树   ****************************
"F1开启和关闭
map <F1> :NERDTreeToggle<CR> 
let NERDTreeChDirMode=1 
let NERDTreeShowBookmarks=1 "显示书签
let NERDTreeIgnore=['\~$', '\.pyc$', '\.swp$'] "设置忽略文件类型
let NERDTreeWinSize=50 "窗口宽度

"*****************   Tagbar   *************************************
"F4开启和关闭
map <F4> :TagbarToggle<CR> 
let g:tagbar_type_markdown = {
    \ 'ctagstype': 'markdown',
    \ 'ctagsbin' : '~/.vim/plugged/markdown2ctags/markdown2ctags.py',
    \ 'ctagsargs' : '-f - --sort=yes ',
    \ 'kinds' : [
        \ 's:sections',
        \ 'i:images'
    \ ],
    \ 'sro' : '|',
    \ 'kind2scope' : {
        \ 's' : 'section',
    \ },
    \ 'sort': 0,
\ }

"*****************   vim-xkbswitch   ******************************
let g:XkbSwitchEnabled     = 1
let g:XkbSwitchIMappings   = ['cn']
let g:XkbSwitchIMappingsTr = {'cn': {'<': '', '>': ''}}

"********************   Markdown-Preview.nvim   *************************
"设置 F3 开启 Markdown 文件预览
map <F3> :MarkdownPreview<CR> 
let g:mkdp_auto_start = 0 "打开文件后自动弹出,0 为否
let g:mkdp_auto_close = 1 "关闭文件后自动关闭预览窗口, 1 为是
let g:mkdp_refresh_slow = 0 "慢速预览, 修改后退出 insert 模式后方会刷新视图, 1 为是
let g:mkdp_open_to_the_world = 0 "开启公网链接, 0 为否
let g:mkdp_browser = '' "指定浏览器, 默认会跟随系统浏览器
let g:mkdp_port = '' " 指定端口, 默认随机端口
let g:mkdp_page_title = '「${name}」' "指定浏览器窗口标题, 默认为 Markdown 文件名

"*********************   LeaderF   ************************
"设置 F2 弹出历史记录, 在 vim 中搜索文件速度太慢, 远不如用 NERDTree
"去ff翻目录所以如果需要搜索的话就是用 Alfred 搜索, Leaderf
"仅作为打开历史jj记录文件工具
nmap <F2> :Leaderf mru<CR>
let g:Lf_UseCache = 0 "不使用缓存
let g:Lf_WindowPosition = 'popup' "默认使用 popup 窗口弹出搜索结果
let g:Lf_PreviewInPopup = 1 "在 popup 窗口中预览文件
let g:Lf_CommandMap = {'<C-K>': ['<Up>'], '<C-J>': ['<Down>']} "使用上下方向按键进行上下移动焦点

"*****************   Syntastic   ***************************
let g:syntastic_error_symbol='>>'
let g:syntastic_warning_symbol='>'
let g:syntastic_check_on_open=0
let g:syntastic_check_on_wq=0
let g:syntastic_javascript_checkers = ['jsl', 'jshint']
let g:syntastic_html_checkers=['tidy', 'jshint']
"let g:syntastic_auto_loc_list = 1  "强制显示错误于下方
let g:syntastic_always_populate_loc_list = 1
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

"********************   YouCompleteMe 设定文件   ***************************
let g:ycm_global_ycm_extra_conf = '~/.ycm_extra_conf.py' " 默认配置文件路径
let g:ycm_confirm_extra_conf=0 "打开vim时不再询问是否加载ycm_extra_conf.py配置
set completeopt=longest,menu
let g:ycm_seed_identifiers_with_syntax=1 "是否开启语义补全
let g:ycm_complete_in_comments=1 "是否在注释中也开启补全"
let g:ycm_collect_identifiers_from_comments_and_strings = 0
let g:ycm_min_num_of_chars_for_completion=2 "开始补全的字符数
let g:ycm_autoclose_preview_window_after_completion=1 "补全后自动关机预览窗口
let g:ycm_cache_omnifunc=0 "禁止缓存匹配项,每次都重新生成匹配项
let g:ycm_complete_in_strings = 1 "字符串中也开启补全
autocmd InsertLeave * if pumvisible() == 0|pclose|endif "离开插入模式后自动关闭预览窗口
"inoremap <expr> <CR>  "回车即选中当前项
"pumvisible() ? '<C-y>' : '\<CR>'
let g:ycm_autoclose_preview_window_after_completion=1

"*********************   seoul256   *****************************************
colo seoul256
" seoul256 (dark):
"   Range:   233 (darkest) ~ 239 (lightest)
"   Default: 235
let g:seoul256_background = 235 "设置背景颜色深度
set background=dark "设置背景颜色为黑色, 必须设置, 否则上面的数值设置没有意义

"*****************   vim-table-mode   **********************
let g:table_mode_corner='|' "设置转角处为 `|`, 如果设为其他比如`+`, Markdown 语法不支持

"*****************   nathanaelkane/vim-indent-guides   ****************
let g:indent_guides_enable_on_vim_startup = 1 "是否在启动时自动运行
let g:indent_guides_guide_size = 1 "设置缩进线宽度, 1 个字符
let g:indent_guides_start_level = 2 "启动等级
let g:indent_guides_exclude_filetypes = ['help', 'nerdtree', 'startify'] "在某些类型中不设置缩进线
let g:indent_guides_auto_colors = 1 "控制缩进线是否使用默认颜色, 最好使用, 否则缩进线中的圆点会被突出显示. 如果确定不使用的话需要在下方命令中指定颜色
"autocmd VimEnter,Colorscheme * :hi IndentGuidesOdd  guibg=#373737   ctermbg=3
"autocmd VimEnter,Colorscheme * :hi IndentGuidesEven guibg=#373737 ctermbg=4

"*****************   mhinz/vim-startify   *****************
" 设置书签
let g:startify_bookmarks            = [
            \ '~/.vimrc',
            \ '~/OneDrive/00_Repo/00_HKMS/00_Inspiration/2020-01-01_QuickRecord.md',
            \ '~/OneDrive/00_Repo/00_HKMS/01_Dev/2019-09-27_总结_iOS_iOS 编程零碎要点',
            \ '~/OneDrive/00_Repo/00_HKMS/01_Dev/2019-12-01_总结_Vim_Vim 编辑器使用.md',
            \ '~/OneDrive/00_Repo/00_HKMS/01_Dev/2019-12-24_总结_Git_版本管理工具 Git 的使用.md',
            \ '~/OneDrive/00_Repo/00_HKMS/01_Dev/2019-12-09_总结_算法_数据结构与算法.md',
            \ '~/OneDrive/00_Repo/00_HKMS/01_Dev/2019-12-10_总结_终端_终端原理及操作.md',
            \]
let g:startify_files_number = 10 "起始页显示的列表长度
let g:startify_session_autoload = 1 "是否自动加载目录下的Session.vim, 很好用

"过滤列表，支持正则表达式
let g:startify_skiplist = [
       \ '\.vimgolf',
       \ '^/tmp',
       \ '/project/.*/documentation',
       \ ]
"页眉区域显示
let g:startify_custom_header = [
\ 'HHHHHHHH     HHHHHHHH                                    llllll                                             ',
\ 'H::::::H     H::::::H                                    l::::l                                             ',
\ 'H::::::H     H::::::H                                    l::::l                                             ',
\ 'HH:::::H     H:::::HH                                    l::::l                                             ',
\ '  H::::H     H::::H     aaaaaaaaaaaa    nnnn nnnnnnnn     l:::l       eeeeeeeeee   yyyyyyy           yyyyyyy',
\ '  H::::H     H::::H     a:::::::::::a   n:::n::::::::n    l:::l     ee::::::::::ee  y:::::y         y:::::y ',
\ '  H:::::HHHHH:::::H     aaaaaaaa:::::a  n:::::::::::::n   l:::l    e:::::eeeee::::ee y:::::y       y:::::y  ',
\ '  H:::::::::::::::H             a::::a  nn:::::::::::::n  l:::l   e:::::e     e::::e  y:::::y     y:::::y   ',
\ '  H:::::::::::::::H       aaaaaa:::::a    n::::nnnn::::n  l:::l   e::::::eeeee:::::e   y:::::y   y:::::y    ',
\ '  H:::::HHHHH:::::H     aa:::::::::::a    n:::n    n:::n  l:::l   e:::::::::::::::e     y:::::y y:::::y     ',
\ '  H::::H     H::::H    a::::aaa::::::a    n:::n    n:::n  l:::l   e:::::eeeeeeeeee       y:::::y:::::y      ',
\ '  H::::H     H::::H   a::::a   a:::::a    n:::n    n:::n  l:::l   e::::::e                y:::::::::y       ',
\ 'HH:::::H     H:::::HH a::::a   a:::::a    n:::n    n:::n l::::::l e:::::::e                y:::::::y        ',
\ 'H::::::H     H::::::H a:::::aaa::::::a    n:::n    n:::n l::::::l  e:::::::eeeeeee          y:::::y         ',
\ 'H::::::H     H::::::H  a:::::::::aa:::a   n:::n    n:::n l::::::l   ee:::::::::::e         y:::::y          ',
\ 'HHHHHHHH     HHHHHHHH   aaaaaaaaa  aaaa   nnnnn    nnnnn llllllll     eeeeeeeeeeee        y:::::y           ',
\ '                                                                                         y:::::y            ',
\ '                                                                                        y:::::y             ',
\ '                                                                                       yyyyyyy              ',
\]
"页脚区域显示
let g:startify_custom_footer = [
\ '+------------------------------+',
\ '|   Swift Python JavaScript    |',
\ '+----------------+-------------+',
\]




"==================   UNUSED   ===================================

"*****************   sheerun/vim-polyglot   ****************
"let g:polyglot_disabled = ['markdown', 'swift'] "禁止 polyglot 对某种类型文件进行渲染

"*****************   Vim-Markdown   ******************************
"let g:vim_markdown_folding_disabled = 0 "关闭折叠功能
"let g:vim_markdown_folding_level = 3 "文件打开时显示的目录总级别
"let g:vim_markdown_folding_style_pythonic = 1
"let g:vim_markdown_conceal = 0 "不隐藏语法, 0 为不隐藏
"let g:vim_markdown_conceal_code_blocks = 0 "设置代码块(行内或段落)不可见, 隐藏代码块选项为独立, 如果不设置的话哪怕设置了不隐藏语法, 代码标记也会被隐藏
"let g:vim_markdown_auto_insert_bullets = 1 "换行时自动插入项目符号
"let g:vim_markdown_strikethrough = 1

"********************   Swift.vim   **************************
"let g:syntastic_swift_checkers = ['swiftpm', 'swiftlint']

"*****************   Limelight   ***************************
"let g:limelight_default_coefficient = 0.5 "设置隐藏区域的黑暗度, 值越大越暗
"let g:limelight_paragraph_span = 2 "设置暗光的跨度, 暗光所能照亮的范围
"let g:limelight_priority = -1 "暗光优先级, 防止搜索的高亮效果被覆盖
"autocmd! User GoyoEnter Limelight "进入 Goyo 专注插件时, 同时开启暗光效果
"autocmd! User GoyoLeave Limelight! "离开 Goyo 专注插件时, 同时退出暗光效果

"*****************   Yggdroot/IndentLine   *****************
"let g:indentLine_setConceal = 0
"let g:indentLine_char = '│' "设置缩进线字符
"let g:indentLine_concealcursor = 'inc' "默认设置, 设置隐藏字符在何种模式下隐藏, i 为 insert, n 为 normal, v 为 visual
"let g:indentLine_conceallevel = 2 "此为默认插件强制且默认选项, 如果设置为0 则缩进线插件不会生效, 等价于设置 set conceallevel=2, 系统一共有四级, 0~3

